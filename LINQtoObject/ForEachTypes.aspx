﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForEachTypes.aspx.cs" Inherits="LINQtoObject.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            height: 38px;
            width: 270px;
        }
        .auto-style5 {
            height: 38px;
            width: 463px;
        }
        .auto-style2 {
            height: 38px;
            width: 331px;
        }
        .auto-style8 {
            width: 270px;
        }
        .auto-style6 {
            width: 463px;
        }
        .auto-style7 {
            width: 331px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1" border="1">
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="btnCreateList" runat="server" Text="Tạo danh sách" OnClick="btnCreateList_Click"/>
                </td>
                <td class="auto-style5">
                    <asp:Button ID="btnCreateList2" runat="server" Text="Tạo danh sách" OnClick="btnCreateList2_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style8">
                    ForEach by LINQ<br />
                    <asp:ListBox ID="lbxSource" runat="server" Height="520px" Width="145px"></asp:ListBox>
                </td>
                <td class="auto-style6">
                    ForEach by Delegate<br />
                    <asp:ListBox ID="lbxSource2" runat="server" Height="520px" Width="145px"></asp:ListBox>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
