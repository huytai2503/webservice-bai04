﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LINQtoObject
{
    public class SinhVien
    {
        public string maSV { get; set; }
        public string tenSV { get; set; }
        public string diaChi { get; set; }
        public int namSinh { get; set; }

        public SinhVien(){}

        public SinhVien(string maSV, string tenSV, string diaChi, int namSinh)
        {
            this.maSV = maSV;
            this.tenSV = tenSV;
            this.diaChi = diaChi;
            this.namSinh = namSinh;
        }
    }
}