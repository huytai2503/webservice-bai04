﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FinderMethods.aspx.cs" Inherits="LINQtoObject.FinderMethods" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1" border="1">
            <tr>
                <td>
                    <asp:Button ID="btnCreateList" runat="server" Text="Tạo DS" OnClick="btnCreateList_Click" />
                &nbsp;
                    <asp:Button ID="btnEreaseList" runat="server" Text="Xóa DS" OnClick="btnEreaseList_Click" />
                    </td>
                <td>
                    <asp:DropDownList ID="dropChonMuc" runat="server" Width="111px">
                        <asp:ListItem Selected="True">maSV</asp:ListItem>
                        <asp:ListItem>tenSV</asp:ListItem>
                        <asp:ListItem>diaChi</asp:ListItem>
                        <asp:ListItem>namSinh</asp:ListItem>
                    </asp:DropDownList>
&nbsp;<asp:DropDownList ID="dropCal" runat="server" Width="50px">
                        <asp:ListItem>=</asp:ListItem>
                        <asp:ListItem>&lt;</asp:ListItem>
                        <asp:ListItem>&gt;</asp:ListItem>
                    </asp:DropDownList>
&nbsp;<asp:TextBox ID="txtIn" runat="server" Width="101px"></asp:TextBox>
                    <br />
                    <asp:Button ID="btnFinder" runat="server" Text="Tìm kiếm" OnClick="btnFinder_Click" />
                &nbsp;
                    <asp:Button ID="btnErease" runat="server" Text="Xóa" />
                &nbsp;
                    <asp:Button ID="btnModifier" runat="server" Text="Sửa" />
                </td>
                <td>
                    <asp:Button ID="btnSkip" runat="server" OnClick="btnSkip_Click" Text="Skip" />
&nbsp;<asp:Button ID="btnSkipWhile" runat="server" Text="SkipWhile" OnClick="btnSkipWhile_Click" />
&nbsp;<asp:Button ID="btnTake" runat="server" Text="Take" OnClick="btnTake_Click" />
&nbsp;<asp:Button ID="btnTakeWhile" runat="server" Text="TakeWhile" OnClick="btnTakeWhile_Click" />
&nbsp;<br />
                    <asp:TextBox ID="txtSkip" runat="server" Width="38px">0</asp:TextBox>
&nbsp;<asp:DropDownList ID="dropCalSkip" runat="server" Width="30px">
                        <asp:ListItem>=</asp:ListItem>
                        <asp:ListItem>&lt;</asp:ListItem>
                        <asp:ListItem>&gt;</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSkipWhile" runat="server" Width="37px">0</asp:TextBox>
&nbsp;<asp:TextBox ID="txtTake" runat="server" Width="40px">0</asp:TextBox>
&nbsp;<asp:DropDownList ID="dropCalTake" runat="server" Width="30px">
                        <asp:ListItem>=</asp:ListItem>
                        <asp:ListItem>&lt;</asp:ListItem>
                        <asp:ListItem>&gt;</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtTakeWhile" runat="server" Width="41px">0</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gridSource" runat="server" Height="186px">
                    </asp:GridView>
&nbsp;
                </td>
                <td>
                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                    <asp:GridView ID="gridTarget" runat="server" Height="127px">
                    </asp:GridView>
                    <asp:Label ID="lblFirstFinder" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblFirstIdx" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblLastFinder" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblLastIdx" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblMinAge" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblMinAgeIdx" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblMaxAge" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblMaxAgeIdx" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:GridView ID="gridTarget2" runat="server" Height="127px">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
