﻿using System;
using System.Collections.Generic;

namespace LINQtoObject
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        List<int> sourceList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCreateList_Click(object sender, EventArgs e)
        {
            Random rd = new Random();
            for (int i = 0; i < 10; i++)
            {
                sourceList.Add(rd.Next(1,100));
            }

            lbxSource.Items.Clear();
            sourceList.ForEach(
                x => 
                    {
                        lbxSource.Items.Add(x + "");
                    }
                );
        }

        protected void btnCreateList2_Click(object sender, EventArgs e)
        {
            Random rd = new Random();
            for (int i = 0; i < 10; i++)
            {
                sourceList.Add(rd.Next(1, 100));
            }

            lbxSource2.Items.Clear();
            sourceList.ForEach(addToListBox);
        }

        protected void addToListBox(int x)
        {
            lbxSource2.Items.Add(x + "");
        }
    }
}