﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LINQtoObject
{
    public static class Class1
    {
        public static string getText(this TextBox tbx)
        {
            string text = tbx.Text;
            return text;
        }

        public static void setText(this TextBox tbx, string text)
        {
            tbx.Text = text;
        }

        public static List<SinhVien> getList(this GridView grid)
        {
            List<SinhVien> list = new List<SinhVien>();
            for (int i = 0; i < grid.Rows.Count; i++)
            {
                SinhVien sv = new SinhVien();
                sv.maSV = grid.Rows[i].Cells[0].Text;
                sv.tenSV = grid.Rows[i].Cells[1].Text;
                sv.diaChi = grid.Rows[i].Cells[2].Text;
                sv.namSinh = int.Parse(grid.Rows[i].Cells[3].Text);
                list.Add(sv);
            }
            return list;
        }

        public static void setSV(this Label lbl, string title, SinhVien sv)
        {
            if (sv != null)
                lbl.Text = title + "[" + sv.maSV + " - " + sv.tenSV + " - " 
                    + sv.diaChi + " - " + sv.namSinh + "]";

        }
    }
}