﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LINQtoObject
{
    public partial class FinderMethods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Tạo sẵn các nội dung cho danh sách
        static string[] dsTen = new string[]
        { "Nga", "Lộc", "Hiền", "An", "Lan", "Loan"};
        static string[] dsHo = new string[]
        { "Trần Huy", "Trần Thị", "La Văn", "La Thị", "Phan Văn", "Phan Thị"};
        static string[] dsDiaChi = new string[]
        { "HoChiMinh", "Phan Thiết", "Đồng Nai", "Phan Rang", "Nha Trang", "Bình Đương"};
        static int[] dsNamSinh = new int[]
        { 1981, 1982, 1978, 2009, 2012, 1987};
        List<SinhVien> dsSV = new List<SinhVien>();

        //Tạo danh sách SV với các nội dung ngẫu nhiên
        protected void btnCreateList_Click(object sender, EventArgs e)
        {
            Random rd = new Random();
            for(int i = 0; i < 20; i++)
            {
                SinhVien sv = new SinhVien();
                sv.maSV = "SV"+i;
                sv.tenSV = dsHo[rd.Next(0,5)] + " " + dsTen[rd.Next(0, 5)];
                sv.diaChi = dsDiaChi[rd.Next(0, 5)];
                sv.namSinh = dsNamSinh[rd.Next(0, 5)];
                dsSV.Add(sv);
            }
            gridSource.DataSource = dsSV;
            gridSource.DataBind();
        }

        //Xóa GridView
        protected void btnEreaseList_Click(object sender, EventArgs e)
        {
            dsSV = null;
            gridSource.DataSource = dsSV;
            gridSource.DataBind();
        }

        //Nút tìm kiếm, nhấn sau khi nhập nội dung cần tìm
        protected void btnFinder_Click(object sender, EventArgs e)
        {
            deleteAllLabel();
            //Hàm getList được tạo ra cho GridView bằng Extension Method trong Class1,
            //để lấy nội dung của GridView set vào List<SinhVien>
            List<SinhVien> sourceList = gridSource.getList();
            List<SinhVien> foundList = new List<SinhVien>();
            //Chọn lựa từ DropBox các column cần tìm theo nội dung nhập vào
            string chosen = dropChonMuc.SelectedValue;
            switch (chosen)
            {
                case "maSV":
                    //Các hàm này trả về một List<SinhVien>
                    foundList = maSVfinder(sourceList);
                    break;
                case "tenSV":
                    foundList = tenSVfinder(sourceList);
                    break;
                case "diaChi":
                    foundList = diaChifinder(sourceList);
                    break;
                case "namSinh":
                    foundList = namSinhfinder(sourceList);
                    break;
            }
            //và set List tìm được vào GridView khác
            gridTarget.DataSource = foundList;
            gridTarget.DataBind();
        }

        //Hàm này xóa tất cả nội dung trong các Label
        protected void deleteAllLabel()
        {
            lblFirstFinder.Text = null;
            lblLastFinder.Text = null;
            lblMinAge.Text = null;
            lblMaxAge.Text = null;
            lblFirstIdx.Text = null;
            lblLastIdx.Text = null;
            lblMinAgeIdx.Text = null;
            lblMaxAgeIdx.Text = null;
        }
        
        //Hàm này để set vào label kết quả tìm thấy bao nhiêu SV hoặc không
        protected void setLabelResult(List<SinhVien> foundList, bool result)
        {
            lblResult.Text = result ?
                "Có " + foundList.Count + " " + dropChonMuc.SelectedValue + ": " + txtIn.Text :
                "Không thấy " + dropChonMuc.SelectedValue + ": " + txtIn.Text;
        }

        //Hàm này để set nội dung về SV lớn và nhỏ tuổi nhất tìm được vào các label,
        protected void setLabelMinMax(List<SinhVien> sourceList, List<SinhVien> foundList, bool result)
        {
            if (result)
            {
                int maxYear = foundList.Max(n => n.namSinh);
                int minYear = foundList.Min(n => n.namSinh);
                SinhVien maxSV = foundList.Find(x => x.namSinh == minYear);
                SinhVien minSV = foundList.Find(x => x.namSinh == maxYear);
                int maxIdx = sourceList.FindIndex(x => x.maSV == maxSV.maSV);
                int minIdx = sourceList.FindIndex(x => x.maSV == minSV.maSV);
                lblMinAge.setSV("SV nhỏ: ", minSV);
                lblMaxAge.setSV("SV lớn: ", maxSV);
                lblMaxAgeIdx.Text = "SV này ở vị trí thứ: " + maxIdx;
                lblMinAgeIdx.Text = "SV này ở vị trí thứ: " + minIdx;
            }
        }

        //Hàm này để set vào những label về các SV đầu và cuối tìm được
        protected void setLabelFirstLast(SinhVien firstSV, SinhVien lastSV, int firstIdx, int lastIdx)
        {
            lblFirstFinder.setSV("SV đầu: ", firstSV);
            lblLastFinder.setSV("SV cuối: ", lastSV);
            lblFirstIdx.Text = "SV này ở vị trí thứ: " + firstIdx;
            lblLastIdx.Text = "SV này ở vị trí thứ: " + lastIdx;
        }

        //Hàm tìm theo mã SV, các hàm sau đây sẽ set vào GridView danh sách tìm được,
        //và set những nội dung khác vào các label
        protected List<SinhVien> maSVfinder(List<SinhVien> sourceList)
        {
            List<SinhVien> foundList = new List<SinhVien>();
            bool result = sourceList.Exists(x => x.maSV.Contains(txtIn.Text));
            foundList = sourceList.FindAll(x => x.maSV.Contains(txtIn.Text));
            setLabelMinMax(sourceList, foundList, result);
            if (result)
            {
                string textInput = txtIn.Text;
                SinhVien svFirst = foundList.Find(x => x.maSV.Contains(textInput));
                SinhVien svLast = foundList.FindLast(x => x.maSV.Contains(textInput));
                int firstIdx = sourceList.FindIndex(x => x.maSV.Contains(textInput));
                int lastIdx = sourceList.FindLastIndex(x => x.maSV.Contains(textInput));
                setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
            }
            return foundList;
        }

        //Hàm tìm theo tên SV, sử dụng FirstOrDefault, LastOrDefault,
        //thay cho Find, FindLast
        protected List<SinhVien> tenSVfinder(List<SinhVien> sourceList)
        {
            List<SinhVien> foundList = new List<SinhVien>();
            bool result = sourceList.Exists(x => x.tenSV.Contains(txtIn.Text));
            foundList = sourceList.FindAll(x => x.tenSV.Contains(txtIn.Text));
            setLabelMinMax(sourceList, foundList, result);
            if (result)
            {
                string textInput = txtIn.Text;
                SinhVien svFirst = foundList.Find(x => x.maSV.Contains(textInput));
                SinhVien svLast = foundList.FindLast(x => x.maSV.Contains(textInput));
                int firstIdx = sourceList.FindIndex(x => x.tenSV.Contains(textInput));
                int lastIdx = sourceList.FindLastIndex(x => x.tenSV.Contains(textInput));
                setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
            }
            return foundList;
        }

        //Hàm tìm theo tên địa chỉ
        protected List<SinhVien> diaChifinder(List<SinhVien> sourceList)
        {
            List<SinhVien> foundList = new List<SinhVien>();
            bool result = sourceList.Exists(x => x.diaChi.Contains(txtIn.Text));
            foundList = sourceList.FindAll(x => x.diaChi.Contains(txtIn.Text));
            setLabelMinMax(sourceList, foundList, result);
            if (result)
            {
                string textInput = txtIn.Text;
                SinhVien svFirst = foundList.Find(x => x.maSV.Contains(textInput));
                SinhVien svLast = foundList.FindLast(x => x.maSV.Contains(textInput));
                int firstIdx = sourceList.FindIndex(x => x.diaChi.Contains(textInput));
                int lastIdx = sourceList.FindLastIndex(x => x.diaChi.Contains(textInput));
                setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
            }
            return foundList;
        }

        //Hàm tìm theo năm sinh sẽ có 3 mục chọn lựa là "<",">" hoặc "="
        protected List<SinhVien> namSinhfinder(List<SinhVien> sourceList)
        {
            List<SinhVien> foundList = new List<SinhVien>();
            bool result = false;
            setLabelMinMax(sourceList, foundList, result);
            //Chọn lựa toán tử để thực hiện tìm kiếm năm sinh
            switch (dropCal.SelectedValue)
            {
                //Nếu kết quả tìm kiếm dựa trên toán tử đã chọn, thì thực hiện khối lệnh theo chọn lựa
                case "<":
                    result = sourceList.Exists(x => x.namSinh < int.Parse(txtIn.Text));
                    if (result)
                    {
                        foundList = sourceList.FindAll(x => x.namSinh < int.Parse(txtIn.Text));
                        string textInput = txtIn.Text;
                        SinhVien svFirst = foundList.Find(x => x.namSinh < int.Parse(textInput));
                        SinhVien svLast = foundList.FindLast(x => x.namSinh < int.Parse(textInput));
                        int firstIdx = sourceList.FindIndex(x => x.namSinh < int.Parse(textInput));
                        int lastIdx = sourceList.FindLastIndex(x => x.namSinh < int.Parse(textInput));
                        setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
                    }
                    break;
                case ">":
                    result = sourceList.Exists(x => x.namSinh > int.Parse(txtIn.Text));
                    if (result)
                    {
                        foundList = sourceList.FindAll(x => x.namSinh > int.Parse(txtIn.Text));
                        string textInput = txtIn.Text;
                        SinhVien svFirst = foundList.Find(x => x.namSinh > int.Parse(textInput));
                        SinhVien svLast = foundList.FindLast(x => x.namSinh > int.Parse(textInput));
                        int firstIdx = sourceList.FindIndex(x => x.namSinh > int.Parse(textInput));
                        int lastIdx = sourceList.FindLastIndex(x => x.namSinh > int.Parse(textInput));
                        setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
                    }
                    break;
                case "=":
                    result = sourceList.Exists(x => x.namSinh == int.Parse(txtIn.Text));
                    if (result)
                    {
                        foundList = sourceList.FindAll(x => x.namSinh == int.Parse(txtIn.Text));
                        string textInput = txtIn.Text;
                        SinhVien svFirst = foundList.Find(x => x.namSinh == int.Parse(textInput));
                        SinhVien svLast = foundList.FindLast(x => x.namSinh == int.Parse(textInput));
                        int firstIdx = sourceList.FindIndex(x => x.namSinh == int.Parse(textInput));
                        int lastIdx = sourceList.FindLastIndex(x => x.namSinh == int.Parse(textInput));
                        setLabelFirstLast(svFirst, svLast, firstIdx, lastIdx);
                    }
                    break;
            }
            return foundList;
        }

        //Tìm cách dùng delegate để thay cho các sự lựa chọn switch case trên đây
        public delegate bool delegateChonMuc(string s);
        public void TimKiem(delegateChonMuc chon)
        {
        }

        //Loại bỏ số lượng n SV đầu tiên khỏi danh sách gốc và set vào GridView khác
        protected void btnSkip_Click(object sender, EventArgs e)
        {
            List<SinhVien> foundList = gridTarget.getList();
            List<SinhVien> skipList = new List<SinhVien>();
            int n = int.Parse(txtSkip.Text);
            skipList.AddRange(foundList.Skip(n));
            gridTarget2.DataSource = skipList;
            gridTarget2.DataBind();
        }

        //Loại bỏ số lượng SV đầu tiên khỏi danh sách gốc theo điều kiện đưa vào,
        //hàm sẽ loại bỏ từng SV từ trên xuống, cho tới khi điều kiện sai thì ngưng
        //sau khi loại bỏ xong thì set vào 1 GridView khác
        protected void btnSkipWhile_Click(object sender, EventArgs e)
        {
            List<SinhVien> foundList = gridTarget.getList();
            List<SinhVien> skipWhileList = new List<SinhVien>();
            int n = int.Parse(txtSkipWhile.Text);
            switch (dropCalSkip.SelectedValue)
            {
                case "=":
                    skipWhileList.AddRange(foundList.SkipWhile(x => x.namSinh == n));
                    break;
                case "<":
                    skipWhileList.AddRange(foundList.SkipWhile(x => x.namSinh < n));
                    break;
                case ">":
                    skipWhileList.AddRange(foundList.SkipWhile(x => x.namSinh > n));
                    break;
            }
            gridTarget2.DataSource = skipWhileList;
            gridTarget2.DataBind();
        }

        //Lấy số lượng n SV đầu tiên từ danh sách gốc và set vào GridView khác
        protected void btnTake_Click(object sender, EventArgs e)
        {
            List<SinhVien> foundList = gridTarget.getList();
            List<SinhVien> takeList = new List<SinhVien>();
            int n = int.Parse(txtTake.Text);
            takeList.AddRange(foundList.Take(n));
            gridTarget2.DataSource = takeList;
            gridTarget2.DataBind();
        }

        //Lấy số lượng SV từ danh sách gốc theo điều kiện đưa vào,
        //hàm sẽ lấy từng SV từ trên xuống, cho tới khi điều kiện sai thì ngưng,
        //sau đó set danh sách mới vào 1 GrdiView khác
        protected void btnTakeWhile_Click(object sender, EventArgs e)
        {
            List<SinhVien> foundList = gridTarget.getList();
            List<SinhVien> takeWhileList = new List<SinhVien>();
            int n = int.Parse(txtTakeWhile.Text);
            switch (dropCalTake.SelectedValue)
            {
                case "=":
                    takeWhileList.AddRange(foundList.TakeWhile(x => x.namSinh == n));
                    break;
                case "<":
                    takeWhileList.AddRange(foundList.TakeWhile(x => x.namSinh < n));
                    break;
                case ">":
                    takeWhileList.AddRange(foundList.TakeWhile(x => x.namSinh > n));
                    break;
            }
            gridTarget2.DataSource = takeWhileList;
            gridTarget2.DataBind();
        }
    }
}