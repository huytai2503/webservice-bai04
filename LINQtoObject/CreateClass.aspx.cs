﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LINQtoObject
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        List<SinhVien> dsSV = new List<SinhVien>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCreateList_Click(object sender, EventArgs e)
        {
            //Cách add to List<> 1
            dsSV.Add(new SinhVien() { maSV = "SV01", tenSV = "Trần Huy Tài" });

            //Cách add to List<> 2
            SinhVien sv2 = new SinhVien() { maSV = "SV02", tenSV = "Trần Huy Lộc" };
            dsSV.Add(sv2);

            //Cách add to List<> 3
            SinhVien sv3 = new SinhVien();
            sv3.maSV = "SV03";
            sv3.tenSV = "Trần Ngọc Thanh Hiền";
            dsSV.Add(sv3);

            lbxSource.Items.Add("Cách add to ListBox 1:");
            for(int i = 0; i < dsSV.Count; i++)
            {
                lbxSource.Items.Add(dsSV[i].maSV+" - "+ dsSV[i].tenSV);
            }
            lbxSource.Items.Add("--------------------");

            lbxSource.Items.Add("Cách add to ListBox 2:");
            foreach (SinhVien sv in dsSV)
            {
                lbxSource.Items.Add(sv.maSV + " - " + sv.tenSV);
            }
            lbxSource.Items.Add("--------------------");

            lbxSource.Items.Add("Cách add to ListBox 3:");
            dsSV.ForEach (
                x =>
                {
                    lbxSource.Items.Add(x.maSV + " - " + x.tenSV);
                });
            lbxSource.Items.Add("--------------------");

            lbxSource.Items.Add("Cách add to ListBox 4:");
            dsSV.ForEach(themSV);
        }

        protected void themSV(SinhVien sv)
        {
            lbxSource.Items.Add(sv.maSV + " - " + sv.tenSV);
        }
    }
}